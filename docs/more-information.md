Tepl - more information
=======================

About versions
--------------

Tepl follows the even/odd minor version scheme.

For example the `6.3.x` versions are development snapshots, and the `6.4.x`
series is stable.

There are sometimes API or ABI breaks, in which case the library soname is
adapted.

Dependencies
------------

- GLib 2
- GTK 3
- GtkSourceView 4
- [Amtk](https://gitlab.gnome.org/World/amtk) 5
- [ICU](http://site.icu-project.org/)

Documentation
-------------

See the `gtk_doc` Meson option. A convenient way to read the API documentation
is with the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) application.

See also other files in this directory for additional notes.

Some links
----------

- [Project home page](https://gitlab.gnome.org/swilmet/tepl)
- [Tarballs](https://download.gnome.org/sources/tepl/)
- [Old Gtef tarballs](https://download.gnome.org/sources/gtef/)

Development and maintenance
---------------------------

If you want to see Tepl improved, you are encouraged to contribute. The
maintainer will do its best at reviewing merge requests. However, contributions
need to follow the roadmap of Tepl or the general philosophy of the project.
